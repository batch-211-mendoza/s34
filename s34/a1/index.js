const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 1.
	app.get("/home", (request, response) => {
		response.send("Welcome to the home page");
	});

// 2.
	// 2.1 - Add users into the users array
		let users = []; 

		app.post("/register", (request, response) => {
			if(request.body.username !== "" && request.body.password !== ""){
				users.push(request.body);
				response.send(`User ${request.body.username} successfully registered!`);
				console.log(request.body);
			} else {
				response.send("Please input BOTH username and password")
			}
		});

	// 2.2 - Print out the users
		app.get("/users", (request, response) => {
			if(users.length !== 0){
				response.send(users);
			} else {
				response.send("There are no users registered.")
			}
		});

// 3. STRETCH-GOAL
	app.delete("/delete-user", (request, response) => {
		let message;

		if(users.length !== 0){
			for(let i = 0; i < users.length; i++){
				if (request.body.username === users[i].username){
					users.splice(i, 1);
					message = `User ${request.body.username} has been deleted`;
					break;
				} else {
					message = "User does not exist.";
				}
			}
		} else {
			message = "User does not exist. No more users to delete.";
		}
		
		response.send(message);
	});

app.listen(port, () => console.log(`Server running at port:${port}`));
